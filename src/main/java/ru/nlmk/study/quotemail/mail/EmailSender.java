package ru.nlmk.study.quotemail.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailSender {

    @Value("${spring.mail.username}")
    private String source;

    private final JavaMailSender javaMailSender;

    public EmailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendEmail(String address, String text, String subject) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(source);
        message.setTo(address);
        message.setText(text);
        message.setSubject(subject);

        javaMailSender.send(message);

        log.info("sent email to {}", address);
    }
}
