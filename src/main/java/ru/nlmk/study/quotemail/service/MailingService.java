package ru.nlmk.study.quotemail.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.study.quotemail.mail.EmailSender;
import ru.nlmk.study.quotemail.model.Quote;

@Service
public class MailingService {

    private final EmailSender emailSender;

    @Autowired
    public MailingService(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void sendMessage(Quote quote) {
        emailSender.sendEmail("koryakovsky.ivan@gmail.com", quote.getText(), quote.getAuthor());
    }
}
