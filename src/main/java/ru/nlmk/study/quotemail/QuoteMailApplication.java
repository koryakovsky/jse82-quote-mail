package ru.nlmk.study.quotemail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteMailApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuoteMailApplication.class, args);
	}

}
