package ru.nlmk.study.quotemail.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.nlmk.study.quotemail.model.Quote;
import ru.nlmk.study.quotemail.service.MailingService;

@Slf4j
@Component
public class QuotesListener {

    private final MailingService mailingService;

    public QuotesListener(MailingService mailingService) {
        this.mailingService = mailingService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void consumeMessage(Quote quote) {
        log.info("received quote: {}", quote.getText());

        mailingService.sendMessage(quote);
    }
}
